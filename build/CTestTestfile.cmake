# CMake generated Testfile for 
# Source directory: /home/oddogan/gitlab/basic-kinematics-using-ros-and-gazebo/src
# Build directory: /home/oddogan/gitlab/basic-kinematics-using-ros-and-gazebo/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("box_gazebo")
subdirs("box_controller")
