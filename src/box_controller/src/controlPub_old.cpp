#include "ros/ros.h"
#include <gazebo_msgs/SetModelState.h>
#include <gazebo_msgs/ModelStates.h>
#include <cstdlib>

gazebo_msgs::ModelState currentstate;

void subFunc(const gazebo_msgs::ModelState& state)
{
    currentstate = state;
}

int main(int argc, char **argv)
{
    ros::init(argc,argv,"robotController");
    ros::NodeHandle n;
    ros::Subscriber stateSub = n.subscribe("calcValue", 1000, &subFunc);
    ros::Publisher statePub = n.advertise<gazebo_msgs::ModelState>("/gazebo/set_model_state", 1000);
    ros::Rate rateCalc(60);

    currentstate.model_name = "cardboard_box";
    currentstate.reference_frame = "world";
    
    while(ros::ok())
    {
        ROS_INFO("%f", currentstate.pose.position.x);
        statePub.publish(currentstate);

        ros::spinOnce();
        rateCalc.sleep();
    }
}
