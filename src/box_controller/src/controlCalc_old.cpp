#include "ros/ros.h"
#include <gazebo_msgs/ModelState.h>
#include <gazebo_msgs/ModelStates.h>

gazebo_msgs::ModelState lastState;

void getLastPos(const gazebo_msgs::ModelStates &states)
{
    lastState.pose = states.pose[1];
    lastState.twist = states.twist[1];
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "KinematicCalculator");
    ros::NodeHandle n;
    ros::Publisher calcValuePub = n.advertise<gazebo_msgs::ModelState>("calcValue", 1000);
    ros::Subscriber lastPos = n.subscribe("/gazebo/model_states", 1000, &getLastPos);
    ros::Rate pubRate(100);

    float force = 0.0001f; // Newtons
    float mass = 1.0f; // Kgs
    float acc = force/mass; // F = m*a -->  a = F/m
    float vel = 0.0f;  // v = a*dt
    float dist = 0.0f; // x = v*dt

    lastState.model_name = "cardboard_box";
    lastState.reference_frame = "world";

    gazebo_msgs::ModelState modelstate;
    modelstate.model_name = "cardboard_box";
    modelstate.reference_frame = "world";

    modelstate.pose = lastState.pose;

    while(ros::ok())
    {
        vel += acc;
        dist += vel;
        modelstate.pose.position.x += dist;
        ROS_INFO("%f", modelstate.pose.position.x);

        calcValuePub.publish(modelstate);

        ros::spinOnce();
        pubRate.sleep();
    }
}