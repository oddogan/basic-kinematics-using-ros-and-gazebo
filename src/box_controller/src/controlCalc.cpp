#include "ros/ros.h"
#include <gazebo_msgs/ModelState.h>
#include <gazebo_msgs/ModelStates.h>

float force = 0.0001f; // Newtons
float mass = 1.0f; // Kgs
float acc = force/mass; // F = m*a -->  a = F/m
float vel = 0.0f;  // v = a*dt
float dist = 0.0f; // x = v*dt
class SubscribeAndPublish
{
public:
  SubscribeAndPublish()
  {
    //Topic you want to publish
    calcValuePub = n.advertise<gazebo_msgs::ModelState>("calcValue", 1000);

    //Topic you want to subscribe
    lastPos = n.subscribe("/gazebo/model_states", 1000, &SubscribeAndPublish::callback, this);
  }

  void callback(const gazebo_msgs::ModelStates &states)
  {
    gazebo_msgs::ModelState lastState;
    lastState.pose = states.pose[1];
    lastState.twist = states.twist[1];
    lastState.model_name = "cardboard_box";
    lastState.reference_frame = "world";
    //.... do something with the input and generate the output...

    gazebo_msgs::ModelState modelstate;
    modelstate.model_name = "cardboard_box";
    modelstate.reference_frame = "world";

    modelstate.pose = lastState.pose;
    vel += acc;
    dist += vel;
    modelstate.pose.position.x += dist;

    ROS_INFO("%f", modelstate.pose.position.x);
    calcValuePub.publish(modelstate);
  }

private:
  ros::NodeHandle n; 
  ros::Publisher calcValuePub;
  ros::Subscriber lastPos;

};//End of class SubscribeAndPublish

int main(int argc, char **argv)
{
    ros::init(argc, argv, "KinematicCalculator");
/*
    ros::Rate pubRate(100);
    while(ros::ok())
    {
        SubscribeAndPublish SAPObject;

        ros::spinOnce();
        pubRate.sleep();
    }
*/
  SubscribeAndPublish SAPObject;
  ros::spin();
}