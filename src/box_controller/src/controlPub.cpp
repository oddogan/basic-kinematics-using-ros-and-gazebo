#include "ros/ros.h"
#include <gazebo_msgs/SetModelState.h>
#include <gazebo_msgs/ModelStates.h>
#include <cstdlib>

class SubscribeAndPublish
{
public:
  SubscribeAndPublish()
  {
    //Topic you want to publish
    statePub = n.advertise<gazebo_msgs::ModelState>("/gazebo/set_model_state", 1000);

    //Topic you want to subscribe
    stateSub = n.subscribe("calcValue", 1000, &SubscribeAndPublish::callback, this);
  }

  void callback(const gazebo_msgs::ModelState& state)
  {
    gazebo_msgs::ModelState currentstate;
    currentstate.model_name = "cardboard_box";
    currentstate.reference_frame = "world";
    //.... do something with the input and generate the output...
    currentstate = state;
    statePub.publish(currentstate);
    ROS_INFO("%f", currentstate.pose.position.x);
  }

private:
  ros::NodeHandle n; 
  ros::Publisher statePub;
  ros::Subscriber stateSub;

};//End of class SubscribeAndPublish

int main(int argc, char **argv)
{
  ros::init(argc,argv,"robotController");
  ros::NodeHandle n;
  ros::Rate rateCalc(60);
    
  /*
  while(ros::ok())
  {
      SubscribeAndPublish SAPObject;
      ros::spinOnce();
      rateCalc.sleep();
  }
  */
 
  SubscribeAndPublish SAPObject;
  ros::spin();
}
